#define VRx A3
#define VRy A1
#define SW  A0

void setup() {
  // put your setup code here, to run once:
  Serial.begin(9600);
  pinMode(VRx,INPUT);
  pinMode(VRy,INPUT);
  pinMode(SW,INPUT_PULLUP);
}

void loop() {
  // put your main code here, to run repeatedly:
  int x = analogRead(VRx);
  int y = analogRead(VRy);
  int btn = digitalRead(SW);
  
  Serial.print(x);
  Serial.print(',');
  Serial.print(y);
  Serial.print(',');
  Serial.println(btn);
  delay(10);

}
