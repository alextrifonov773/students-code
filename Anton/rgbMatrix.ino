
#include<Adafruit_NeoPixel.h>
#define RGB_Matrix_PIN A3
int rgbMatrixPixelCount= 64;
Adafruit_NeoPixel rgbMatrix= Adafruit_NeoPixel(rgbMatrixPixelCount,RGB_Matrix_PIN,NEO_GRB+NEO_KHZ800);
void setup() {
  // put your setup code here, to run once:

  int brightness = 10;
  rgbMatrix.begin();
  rgbMatrix.setBrightness(brightness);
}

void loop() {
  int asdfg=1;
  // put your main code here, to run repeatedly:
    for(int i=0;i<64;i++){
      rgbMatrix.setPixelColor(i,asdfg++,asdfg--,asdfg*2);
      if(asdfg>64){
        asdfg=1;
        }
      rgbMatrix.show();
      }
}
