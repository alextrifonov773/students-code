#include <Adafruit_NeoPixel.h>
#define RGB_Matrix A3
Adafruit_NeoPixel pixels_RGB_Matrix = Adafruit_NeoPixel(64, RGB_Matrix, NEO_GRB + NEO_KHZ800);
void setup() {
  pinMode(RGB_Matrix, OUTPUT);
  pixels_RGB_Matrix.begin();
  pixels_RGB_Matrix.setBrightness(70);
}

void loop() {
  for(int i = 0;i < 64;i++){
          if(i < 8){
          pixels_RGB_Matrix.setPixelColor(i, 255, 0, 0); //RGB
              pixels_RGB_Matrix.setPixelColor(i+8, 255, 70, 0);//orange
              pixels_RGB_Matrix.setPixelColor(i+16, 255, 255, 0);//yellow
              pixels_RGB_Matrix.setPixelColor(i+24, 0, 255, 59);//green-emerald
              pixels_RGB_Matrix.setPixelColor(i+32, 162, 97, 0);//aqua
              pixels_RGB_Matrix.setPixelColor(i+40, 0, 0, 153);//blue-sapphire
              pixels_RGB_Matrix.setPixelColor(i+48, 250, 0, 100);//purple-fuschia
              pixels_RGB_Matrix.setPixelColor(i+56, 65, 0, 65);//purple-royal
              pixels_RGB_Matrix.show();
          pixels_RGB_Matrix.show();
          delay(200);
          }
          if(i >= 8 && i <= 15){
              pixels_RGB_Matrix.setPixelColor(i, 255, 0, 0);
              pixels_RGB_Matrix.setPixelColor(i+8, 255, 70, 0);//orange
              pixels_RGB_Matrix.setPixelColor(i+16, 255, 255, 0);//yellow
              pixels_RGB_Matrix.setPixelColor(i+24, 0, 255, 59);//green-emerald
              pixels_RGB_Matrix.setPixelColor(i+32, 162, 97, 0);//aqua
              pixels_RGB_Matrix.setPixelColor(i+40, 0, 0, 153);//blue-sapphire
              pixels_RGB_Matrix.setPixelColor(i+48, 250, 0, 100);//purple-fuschia
              pixels_RGB_Matrix.setPixelColor(i-8, 65, 0, 65);//purple-royal
              pixels_RGB_Matrix.show();
              delay(200);
          }
          if(i > 15 && i <= 23){
              pixels_RGB_Matrix.setPixelColor(i, 255, 0, 0);
              pixels_RGB_Matrix.setPixelColor(i+8, 255, 70, 0);// orange
              pixels_RGB_Matrix.setPixelColor(i+16, 255, 255, 0);//yellow
              pixels_RGB_Matrix.setPixelColor(i+24, 0, 255, 59);//green-emerald
              pixels_RGB_Matrix.setPixelColor(i+32, 162, 97, 0);//aqua
              pixels_RGB_Matrix.setPixelColor(i+40, 0, 0, 153);//blue-sapphire
              pixels_RGB_Matrix.setPixelColor(i-16, 250, 0, 100);//purple-fuschia
              pixels_RGB_Matrix.setPixelColor(i-8, 65, 0, 65);//purple-royal
              pixels_RGB_Matrix.show();
              delay(200);
              }
          if(i > 23 && i <= 31){
              pixels_RGB_Matrix.setPixelColor(i, 255, 0, 0);
              pixels_RGB_Matrix.setPixelColor(i+8, 255, 70, 0);//orange
              pixels_RGB_Matrix.setPixelColor(i+16, 255, 255, 0);//yellow
              pixels_RGB_Matrix.setPixelColor(i+24, 0, 255, 59);//green-emerald
              pixels_RGB_Matrix.setPixelColor(i+32, 162, 97, 0);//aqua
              pixels_RGB_Matrix.setPixelColor(i-24, 0, 0, 153);//blue-sapphire
              pixels_RGB_Matrix.setPixelColor(i-16, 250, 0, 100);//purple-fuschia
              pixels_RGB_Matrix.setPixelColor(i-8, 65, 0, 65);//purple-royal
              pixels_RGB_Matrix.show();
              delay(200);
            }
          if(i > 31 && i <= 39){
              pixels_RGB_Matrix.setPixelColor(i, 255, 0, 0);
              pixels_RGB_Matrix.setPixelColor(i+8, 255, 70, 0);// orange
              pixels_RGB_Matrix.setPixelColor(i+16, 255, 255, 0);//yellow
              pixels_RGB_Matrix.setPixelColor(i+24, 0, 255, 59);//green-emerald
              pixels_RGB_Matrix.setPixelColor(i-32, 162, 97, 0);//aqua
              pixels_RGB_Matrix.setPixelColor(i-24, 0, 0, 153);//blue-sapphire
              pixels_RGB_Matrix.setPixelColor(i-16, 250, 0, 100);//purple-fuschia
              pixels_RGB_Matrix.setPixelColor(i-8, 65, 0, 65);//purple-royal
              pixels_RGB_Matrix.show();
              delay(200);
          }
          if(i > 39 && i <= 47){
              pixels_RGB_Matrix.setPixelColor(i, 255, 0, 0);
              pixels_RGB_Matrix.setPixelColor(i+8, 255, 70, 0);//orange
              pixels_RGB_Matrix.setPixelColor(i+16, 255, 255, 0);//yellow
              pixels_RGB_Matrix.setPixelColor(i-40, 0, 255, 59);//green-emerald
              pixels_RGB_Matrix.setPixelColor(i-32, 162, 97, 0);//aqua
              pixels_RGB_Matrix.setPixelColor(i-24, 0, 0, 153);//blue-sapphire
              pixels_RGB_Matrix.setPixelColor(i-16, 250, 0, 100);//purple-fuschia
              pixels_RGB_Matrix.setPixelColor(i-8, 65, 0, 65);//purple-royal
              pixels_RGB_Matrix.show();
              delay(200);
          }
          if(i > 47 && i <= 55){
              pixels_RGB_Matrix.setPixelColor(i, 255, 0, 0);
              pixels_RGB_Matrix.setPixelColor(i+8, 255, 70, 0);//orange
              pixels_RGB_Matrix.setPixelColor(i-48, 255, 255, 0);//yellow
              pixels_RGB_Matrix.setPixelColor(i-40, 0, 255, 59);//green-emerald
              pixels_RGB_Matrix.setPixelColor(i-32, 162, 97, 0);//aqua
              pixels_RGB_Matrix.setPixelColor(i-24, 0, 0, 153);//blue-sapphire
              pixels_RGB_Matrix.setPixelColor(i-16, 250, 0, 100);//purple-fuschia
              pixels_RGB_Matrix.setPixelColor(i-8, 65, 0, 65);//purple-royal
              pixels_RGB_Matrix.show();
              delay(200);
          }
          if(i > 55 && i < 64){
              pixels_RGB_Matrix.setPixelColor(i, 255, 0, 0);
              pixels_RGB_Matrix.setPixelColor(i-56, 255, 70, 0);//orange
              pixels_RGB_Matrix.setPixelColor(i-48, 255, 255, 0);//yellow
              pixels_RGB_Matrix.setPixelColor(i-40, 0, 255, 59);//green-emerald
              pixels_RGB_Matrix.setPixelColor(i-32, 162, 97, 0);//aqua
              pixels_RGB_Matrix.setPixelColor(i-24, 0, 0, 153);//blue-sapphire
              pixels_RGB_Matrix.setPixelColor(i-16, 250, 0, 100);//purple-fuschia
              pixels_RGB_Matrix.setPixelColor(i-8, 65, 0, 65);//purple-royal
              pixels_RGB_Matrix.show();
              delay(200);
          }
   }
}
