#include <Adafruit_NeoPixel.h>

Adafruit_NeoPixel matrix = Adafruit_NeoPixel (64, A0, NEO_GRB + NEO_KHZ800);
void setup() {
  matrix.begin();
  matrix.setBrightness(70);
  pinMode(A0, OUTPUT);
}
int index = 0;
int Button_State = 0;
void loop() {
  if (index >= 64) {
    matrix.clear();
    index = 0;

  }


  if (digitalRead(12) == HIGH && Button_State == 0)
  {
    Button_State = 1;

    matrix.setPixelColor(index - 1, 0, 0, 0);
    matrix.setPixelColor(index, 0, 0, 255);
    matrix.show();
    index++;
  }

  else if (digitalRead(12) == LOW)
  {
    Button_State = 0;
  }
  // for (int Cords = 63; Cords >= 0; Cords--) {
  //matrix.setPixelColor( Cords, 0, 255, 0);
  // matrix.show();
  // delay(100);
  //matrix.clear();

}
