#include <Adafruit_NeoPixel.h>
#define RGB_Matrix_PIN A3
#define rgbMatrixPixelsCount 64
Adafruit_NeoPixel rgbMatrix = Adafruit_NeoPixel(rgbMatrixPixelsCount,RGB_Matrix_PIN, NEO_GRB + NEO_KHZ800);
 
void setup() {
  rgbMatrix.begin();
  rgbMatrix.setBrightness(5);
}

void loop() {
  for( int i = 63; i>=0; i--)
  {
  rgbMatrix.setPixelColor( i, 255, 255, 255);
  rgbMatrix.show(); 
  delay(200);
  }

}
