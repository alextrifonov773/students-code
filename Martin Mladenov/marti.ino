#define RGBMatrix A0
#define RX 3
#define TX 2

#include <Adafruit_NeoPixel.h>
#include <SoftwareSerial.h>
#include"VirtuinoBluetooth.h"

Adafruit_NeoPixel matrix(64, RGBMatrix, NEO_RGB + NEO_KHZ800);
SoftwareSerial bluetooth(RX, TX);
VirtuinoBluetooth virtuino(bluetooth);



void setup() {
  pinMode(RGBMatrix, OUTPUT);
  matrix.setBrightness(60);
  matrix.begin();
  bluetooth.begin(9600);
  
}
int pixelNum = 0;
int travel_speed = 100;

void loop() {
  virtuino.run();
  travel_speed = virtuino.vMemoryRead(2);

 if(pixelNum > 63){
  matrix.clear();
  pixelNum = 0;
  }

if(virtuino.vMemoryRead(1) == 1){
  matrix.setPixelColor(pixelNum, 23, 34, 45);
  matrix.show();
  pixelNum++;
  delay(travel_speed);
  }
 
}
