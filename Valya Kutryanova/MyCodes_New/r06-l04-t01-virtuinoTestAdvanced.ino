#include <Adafruit_NeoPixel.h>

#include "VirtuinoBluetooth.h"
#include "mega2560_pinout.h"

//#define WP18 (18)
//#define DP18 WP18
//#define TX1 WP18
//#define TX_BLUETOOTH_PIN WP18
//
//#define WP19 (19)
//#define DP19 WP19
//#define RX1 WP19
//#define RX_BLUETOOTH_PIN WP19

VirtuinoBluetooth virtuino(Serial1);


// Initialize all needed pins and libraries
void setup() {
  Serial.begin(9600);
  Serial1.begin(9600);
  
  Serial.println("Start");

  virtuino.vPinMode(BIG_BUTTON_PIN, INPUT);
  
  virtuino.vPinMode(BLUE_LED_PIN, OUTPUT);
  virtuino.vPinMode(RED_LED_PIN, OUTPUT);
  virtuino.vPinMode(GREEN_LED_PIN, OUTPUT);
}

int mem1 = 0;

// Write the code executed in a loop
void loop() {
  virtuino.run();

  int mem = virtuino.vMemoryRead(1);

  if (mem != mem1) {
    mem1 = mem;
    Serial.print("M[1]=");
    Serial.println(mem1);

    if (mem == 1) {
      int myValueWrite = random(0, 256);
      Serial.print("M[7]=");
      Serial.println(myValueWrite);

      analogWrite(GREEN_LED_PIN, myValueWrite);
    
      virtuino.vMemoryWrite(7, myValueWrite);
    }
    else{
      analogWrite(GREEN_LED_PIN, LOW);
    }    
  }
    
}
