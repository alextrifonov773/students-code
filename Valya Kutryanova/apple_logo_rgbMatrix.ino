#include <Adafruit_NeoPixel.h>
Adafruit_NeoPixel pixel(64, A0, NEO_GRB + NEO_KHZ800);
void setup() {
  pinMode(12, INPUT);
  pinMode(A0, OUTPUT);
  pixel.begin();
  pixel.setBrightness(15);

}
int pixelBrightness = 0;
void loop() {
  int tablica[8][8] ;
  int i = 4;
  int j = 3;
  pixel.setPixelColor(8 * i + j, 255, 255, 255);
  pixel.show();
  i = 3;
  j = 3;
  pixel.setPixelColor(8 * i + j, 255, 255, 255);
  pixel.show();
  i = 4;
  j = 4;
  pixel.setPixelColor(8 * i + j, 255, 255, 255);
  pixel.show();
}
