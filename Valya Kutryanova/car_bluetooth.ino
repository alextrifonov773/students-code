#define TX 2
#define RX 3
#define Motor_A_IN1 11
#define Motor_A_IN2 5
#define Motor_B_IN3 9
#define Motor_B_IN4 10
#define my_delay 200


#include<SoftwareSerial.h>
SoftwareSerial bluetooth(RX, TX);

void setup() {
  Serial.begin(9600);

  bluetooth.begin(9600);
}


//bluetooth.available() &&

void loop() {
  int variable = bluetooth.read();
  if (variable == '1') {
    forward();
    delay(my_delay);
  }
  else if (variable == '2') {
    backward();
    delay(my_delay);
  }
  else if (variable == '3') {
    turnright();
    delay(my_delay);
  }
  else if (variable == '4') {
    turnleft();
    delay(my_delay);
  }
  else if (variable == '5'){
    stopmotor();
    delay(my_delay);
  }
}

void forward() {
  analogWrite(Motor_A_IN1, 250);
  analogWrite(Motor_A_IN2, 0);
  analogWrite(Motor_B_IN3, 250);
  analogWrite(Motor_B_IN4, 0);
}

void backward() {
  analogWrite(Motor_A_IN1, 0);
  analogWrite(Motor_A_IN2, 250);
  analogWrite(Motor_B_IN3, 0);
  analogWrite(Motor_B_IN4, 250);
}

void turnright() {
  analogWrite(Motor_A_IN1, 250);
  analogWrite(Motor_A_IN2, 0);
  analogWrite(Motor_B_IN3, 0);
  analogWrite(Motor_B_IN4, 0);
}

void turnleft() {
  analogWrite(Motor_A_IN1, 0);
  analogWrite(Motor_A_IN2, 0);
  analogWrite(Motor_B_IN3, 250);
  analogWrite(Motor_B_IN4, 0);
}

void stopmotor() {
  analogWrite(Motor_A_IN1, 0);
  analogWrite(Motor_A_IN2, 0);
  analogWrite(Motor_B_IN3, 0);
  analogWrite(Motor_B_IN4, 0);
}
